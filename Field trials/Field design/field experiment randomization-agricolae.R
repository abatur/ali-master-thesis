rm(list=ls())
setwd
getwd()
install.packages("agricolae")
library(agricolae)
y
detach(package:agricolae) # detach package agricole
designs<-apropos("design")
print(designs[substr(designs,1,6)=="design"], row.names=FALSE)
library(agricolae)
library(agricolae)
# 5 treatments and 6 blocks
trt<-c("A","B","C","D","E")
outdesign <-design.rcbd(trt,6,serie=2,986,"Wichmann-Hill") # seed = 986
book <-outdesign$book # field book
# write in hard disk
# write.table(book,"rcbd.txt", row.names=FALSE, sep="\t")
# file.show("rcbd.txt")
# Plots in field model ZIGZAG
fieldbook <- zigzag(outdesign)
print(outdesign$sketch)
print(matrix(fieldbook[,1],byrow=TRUE,ncol=5))
# continuous numbering of plot
outdesign <-design.rcbd(trt,6,serie=0,continue=TRUE)
head(outdesign$book)
 
#My part-Location HDH
trt<-c("1396","1115","1283","1241","1280", "1231","1299", "1101" , "1245", "1290", "Bärnkrafft") 
outdesignHDH <-design.rcbd(trt,3,serie=1,986,"Wichmann-Hill") # seed = 986
bookHDH <-outdesignHDH$book # field book
fieldbookHDH <- zigzag(outdesignHDH)
print(outdesignHDH$sketch)

# Location EW
outdesignEW <-design.rcbd(trt,3,serie=1,continue=TRUE)
bookEW <-outdesignEW$book$EW # field book
fieldbookEW <- zigzag(outdesignEW)
print(outdesignEW$sketch)

# Location IH
outdesignIH <-design.rcbd(trt,3,serie=1,continue=TRUE)
bookIH <-outdesignIH$book$IH # field book
fieldbookIH <- zigzag(outdesignIH)
print(outdesignIH$sketch)

# Location OLI
outdesignOLI <-design.rcbd(trt,3,serie=1,continue=TRUE)
bookOLI <-outdesignOLI$book$OLI # field book
fieldbookOLI <- zigzag(outdesignOLI)
print(outdesignOLI$sketch)
